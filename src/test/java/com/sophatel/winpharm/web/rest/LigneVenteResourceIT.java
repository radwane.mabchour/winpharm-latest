package com.sophatel.winpharm.web.rest;

import com.sophatel.winpharm.WinpharmApp;
import com.sophatel.winpharm.domain.LigneVente;
import com.sophatel.winpharm.repository.LigneVenteRepository;
import com.sophatel.winpharm.service.LigneVenteService;
import com.sophatel.winpharm.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.sophatel.winpharm.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link LigneVenteResource} REST controller.
 */
@SpringBootTest(classes = WinpharmApp.class)
public class LigneVenteResourceIT {

    private static final String DEFAULT_DESIGNATION_PRODUIT = "AAAAAAAAAA";
    private static final String UPDATED_DESIGNATION_PRODUIT = "BBBBBBBBBB";

    private static final Double DEFAULT_PRIX_PRODUIT = 1D;
    private static final Double UPDATED_PRIX_PRODUIT = 2D;
    private static final Double SMALLER_PRIX_PRODUIT = 1D - 1D;

    private static final Long DEFAULT_QUANTITE = 0L;
    private static final Long UPDATED_QUANTITE = 1L;
    private static final Long SMALLER_QUANTITE = 0L - 1L;

    private static final Double DEFAULT_SOUS_TOTAL = 0D;
    private static final Double UPDATED_SOUS_TOTAL = 1D;
    private static final Double SMALLER_SOUS_TOTAL = 0D - 1D;

    private static final Double DEFAULT_SOUS_TOTAL_HT = 0D;
    private static final Double UPDATED_SOUS_TOTAL_HT = 1D;
    private static final Double SMALLER_SOUS_TOTAL_HT = 0D - 1D;

    @Autowired
    private LigneVenteRepository ligneVenteRepository;

    @Autowired
    private LigneVenteService ligneVenteService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restLigneVenteMockMvc;

    private LigneVente ligneVente;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final LigneVenteResource ligneVenteResource = new LigneVenteResource(ligneVenteService);
        this.restLigneVenteMockMvc = MockMvcBuilders.standaloneSetup(ligneVenteResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LigneVente createEntity(EntityManager em) {
        LigneVente ligneVente = new LigneVente()
            .designationProduit(DEFAULT_DESIGNATION_PRODUIT)
            .prixProduit(DEFAULT_PRIX_PRODUIT)
            .quantite(DEFAULT_QUANTITE)
            .sousTotal(DEFAULT_SOUS_TOTAL)
            .sousTotalHT(DEFAULT_SOUS_TOTAL_HT);
        return ligneVente;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LigneVente createUpdatedEntity(EntityManager em) {
        LigneVente ligneVente = new LigneVente()
            .designationProduit(UPDATED_DESIGNATION_PRODUIT)
            .prixProduit(UPDATED_PRIX_PRODUIT)
            .quantite(UPDATED_QUANTITE)
            .sousTotal(UPDATED_SOUS_TOTAL)
            .sousTotalHT(UPDATED_SOUS_TOTAL_HT);
        return ligneVente;
    }

    @BeforeEach
    public void initTest() {
        ligneVente = createEntity(em);
    }

    @Test
    @Transactional
    public void createLigneVente() throws Exception {
        int databaseSizeBeforeCreate = ligneVenteRepository.findAll().size();

        // Create the LigneVente
        restLigneVenteMockMvc.perform(post("/api/ligne-ventes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ligneVente)))
            .andExpect(status().isCreated());

        // Validate the LigneVente in the database
        List<LigneVente> ligneVenteList = ligneVenteRepository.findAll();
        assertThat(ligneVenteList).hasSize(databaseSizeBeforeCreate + 1);
        LigneVente testLigneVente = ligneVenteList.get(ligneVenteList.size() - 1);
        assertThat(testLigneVente.getDesignationProduit()).isEqualTo(DEFAULT_DESIGNATION_PRODUIT);
        assertThat(testLigneVente.getPrixProduit()).isEqualTo(DEFAULT_PRIX_PRODUIT);
        assertThat(testLigneVente.getQuantite()).isEqualTo(DEFAULT_QUANTITE);
        assertThat(testLigneVente.getSousTotal()).isEqualTo(DEFAULT_SOUS_TOTAL);
        assertThat(testLigneVente.getSousTotalHT()).isEqualTo(DEFAULT_SOUS_TOTAL_HT);
    }

    @Test
    @Transactional
    public void createLigneVenteWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = ligneVenteRepository.findAll().size();

        // Create the LigneVente with an existing ID
        ligneVente.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLigneVenteMockMvc.perform(post("/api/ligne-ventes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ligneVente)))
            .andExpect(status().isBadRequest());

        // Validate the LigneVente in the database
        List<LigneVente> ligneVenteList = ligneVenteRepository.findAll();
        assertThat(ligneVenteList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkQuantiteIsRequired() throws Exception {
        int databaseSizeBeforeTest = ligneVenteRepository.findAll().size();
        // set the field null
        ligneVente.setQuantite(null);

        // Create the LigneVente, which fails.

        restLigneVenteMockMvc.perform(post("/api/ligne-ventes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ligneVente)))
            .andExpect(status().isBadRequest());

        List<LigneVente> ligneVenteList = ligneVenteRepository.findAll();
        assertThat(ligneVenteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllLigneVentes() throws Exception {
        // Initialize the database
        ligneVenteRepository.saveAndFlush(ligneVente);

        // Get all the ligneVenteList
        restLigneVenteMockMvc.perform(get("/api/ligne-ventes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ligneVente.getId().intValue())))
            .andExpect(jsonPath("$.[*].designationProduit").value(hasItem(DEFAULT_DESIGNATION_PRODUIT.toString())))
            .andExpect(jsonPath("$.[*].prixProduit").value(hasItem(DEFAULT_PRIX_PRODUIT.doubleValue())))
            .andExpect(jsonPath("$.[*].quantite").value(hasItem(DEFAULT_QUANTITE.intValue())))
            .andExpect(jsonPath("$.[*].sousTotal").value(hasItem(DEFAULT_SOUS_TOTAL.doubleValue())))
            .andExpect(jsonPath("$.[*].sousTotalHT").value(hasItem(DEFAULT_SOUS_TOTAL_HT.doubleValue())));
    }
    
    @Test
    @Transactional
    public void getLigneVente() throws Exception {
        // Initialize the database
        ligneVenteRepository.saveAndFlush(ligneVente);

        // Get the ligneVente
        restLigneVenteMockMvc.perform(get("/api/ligne-ventes/{id}", ligneVente.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(ligneVente.getId().intValue()))
            .andExpect(jsonPath("$.designationProduit").value(DEFAULT_DESIGNATION_PRODUIT.toString()))
            .andExpect(jsonPath("$.prixProduit").value(DEFAULT_PRIX_PRODUIT.doubleValue()))
            .andExpect(jsonPath("$.quantite").value(DEFAULT_QUANTITE.intValue()))
            .andExpect(jsonPath("$.sousTotal").value(DEFAULT_SOUS_TOTAL.doubleValue()))
            .andExpect(jsonPath("$.sousTotalHT").value(DEFAULT_SOUS_TOTAL_HT.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingLigneVente() throws Exception {
        // Get the ligneVente
        restLigneVenteMockMvc.perform(get("/api/ligne-ventes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLigneVente() throws Exception {
        // Initialize the database
        ligneVenteService.save(ligneVente);

        int databaseSizeBeforeUpdate = ligneVenteRepository.findAll().size();

        // Update the ligneVente
        LigneVente updatedLigneVente = ligneVenteRepository.findById(ligneVente.getId()).get();
        // Disconnect from session so that the updates on updatedLigneVente are not directly saved in db
        em.detach(updatedLigneVente);
        updatedLigneVente
            .designationProduit(UPDATED_DESIGNATION_PRODUIT)
            .prixProduit(UPDATED_PRIX_PRODUIT)
            .quantite(UPDATED_QUANTITE)
            .sousTotal(UPDATED_SOUS_TOTAL)
            .sousTotalHT(UPDATED_SOUS_TOTAL_HT);

        restLigneVenteMockMvc.perform(put("/api/ligne-ventes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedLigneVente)))
            .andExpect(status().isOk());

        // Validate the LigneVente in the database
        List<LigneVente> ligneVenteList = ligneVenteRepository.findAll();
        assertThat(ligneVenteList).hasSize(databaseSizeBeforeUpdate);
        LigneVente testLigneVente = ligneVenteList.get(ligneVenteList.size() - 1);
        assertThat(testLigneVente.getDesignationProduit()).isEqualTo(UPDATED_DESIGNATION_PRODUIT);
        assertThat(testLigneVente.getPrixProduit()).isEqualTo(UPDATED_PRIX_PRODUIT);
        assertThat(testLigneVente.getQuantite()).isEqualTo(UPDATED_QUANTITE);
        assertThat(testLigneVente.getSousTotal()).isEqualTo(UPDATED_SOUS_TOTAL);
        assertThat(testLigneVente.getSousTotalHT()).isEqualTo(UPDATED_SOUS_TOTAL_HT);
    }

    @Test
    @Transactional
    public void updateNonExistingLigneVente() throws Exception {
        int databaseSizeBeforeUpdate = ligneVenteRepository.findAll().size();

        // Create the LigneVente

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLigneVenteMockMvc.perform(put("/api/ligne-ventes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ligneVente)))
            .andExpect(status().isBadRequest());

        // Validate the LigneVente in the database
        List<LigneVente> ligneVenteList = ligneVenteRepository.findAll();
        assertThat(ligneVenteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteLigneVente() throws Exception {
        // Initialize the database
        ligneVenteService.save(ligneVente);

        int databaseSizeBeforeDelete = ligneVenteRepository.findAll().size();

        // Delete the ligneVente
        restLigneVenteMockMvc.perform(delete("/api/ligne-ventes/{id}", ligneVente.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<LigneVente> ligneVenteList = ligneVenteRepository.findAll();
        assertThat(ligneVenteList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(LigneVente.class);
        LigneVente ligneVente1 = new LigneVente();
        ligneVente1.setId(1L);
        LigneVente ligneVente2 = new LigneVente();
        ligneVente2.setId(ligneVente1.getId());
        assertThat(ligneVente1).isEqualTo(ligneVente2);
        ligneVente2.setId(2L);
        assertThat(ligneVente1).isNotEqualTo(ligneVente2);
        ligneVente1.setId(null);
        assertThat(ligneVente1).isNotEqualTo(ligneVente2);
    }
}
