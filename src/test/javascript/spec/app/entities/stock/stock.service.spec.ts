import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { StockService } from 'app/entities/stock/stock.service';
import { IStock, Stock } from 'app/shared/model/stock.model';

describe('Service Tests', () => {
  describe('Stock Service', () => {
    let injector: TestBed;
    let service: StockService;
    let httpMock: HttpTestingController;
    let elemDefault: IStock;
    let expectedResult;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(StockService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new Stock(0, 0, 0, 0, currentDate, 0, 0);
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            datePeremption: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a Stock', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            datePeremption: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            datePeremption: currentDate
          },
          returnedFromService
        );
        service
          .create(new Stock(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a Stock', () => {
        const returnedFromService = Object.assign(
          {
            couvertureMin: 1,
            couvertureMax: 1,
            quantite: 1,
            datePeremption: currentDate.format(DATE_FORMAT),
            prixAchat: 1,
            prixVente: 1
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            datePeremption: currentDate
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of Stock', () => {
        const returnedFromService = Object.assign(
          {
            couvertureMin: 1,
            couvertureMax: 1,
            quantite: 1,
            datePeremption: currentDate.format(DATE_FORMAT),
            prixAchat: 1,
            prixVente: 1
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            datePeremption: currentDate
          },
          returnedFromService
        );
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Stock', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
