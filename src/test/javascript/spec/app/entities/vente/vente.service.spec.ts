import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { VenteService } from 'app/entities/vente/vente.service';
import { IVente, Vente } from 'app/shared/model/vente.model';

describe('Service Tests', () => {
  describe('Vente Service', () => {
    let injector: TestBed;
    let service: VenteService;
    let httpMock: HttpTestingController;
    let elemDefault: IVente;
    let expectedResult;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(VenteService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new Vente(0, currentDate, 0, 0);
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            dateVente: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a Vente', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            dateVente: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            dateVente: currentDate
          },
          returnedFromService
        );
        service
          .create(new Vente(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a Vente', () => {
        const returnedFromService = Object.assign(
          {
            dateVente: currentDate.format(DATE_FORMAT),
            totalHT: 1,
            totalTTC: 1
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dateVente: currentDate
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of Vente', () => {
        const returnedFromService = Object.assign(
          {
            dateVente: currentDate.format(DATE_FORMAT),
            totalHT: 1,
            totalTTC: 1
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            dateVente: currentDate
          },
          returnedFromService
        );
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Vente', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
