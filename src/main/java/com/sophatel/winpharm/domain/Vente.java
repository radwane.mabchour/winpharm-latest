package com.sophatel.winpharm.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A Vente.
 */
@Entity
@Table(name = "vente")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Vente implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "date_vente", nullable = false)
    private LocalDate dateVente;

    @NotNull
    @DecimalMin(value = "0")
    @Column(name = "total_ht", nullable = false)
    private Double totalHT;

    @NotNull
    @DecimalMin(value = "0")
    @Column(name = "total_ttc", nullable = false)
    private Double totalTTC;

    @OneToMany(mappedBy = "vente", cascade = CascadeType.REMOVE)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<LigneVente> ligneVentes = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateVente() {
        return dateVente;
    }

    public Vente dateVente(LocalDate dateVente) {
        this.dateVente = dateVente;
        return this;
    }

    public void setDateVente(LocalDate dateVente) {
        this.dateVente = dateVente;
    }

    public Double getTotalHT() {
        return totalHT;
    }

    public Vente totalHT(Double totalHT) {
        this.totalHT = totalHT;
        return this;
    }

    public void setTotalHT(Double totalHT) {
        this.totalHT = totalHT;
    }

    public Double getTotalTTC() {
        return totalTTC;
    }

    public Vente totalTTC(Double totalTTC) {
        this.totalTTC = totalTTC;
        return this;
    }

    public void setTotalTTC(Double totalTTC) {
        this.totalTTC = totalTTC;
    }

    public Set<LigneVente> getLigneVentes() {
        return ligneVentes;
    }

    public Vente ligneVentes(Set<LigneVente> ligneVentes) {
        this.ligneVentes = ligneVentes;
        return this;
    }

    public Vente addLigneVente(LigneVente ligneVente) {
        this.ligneVentes.add(ligneVente);
        ligneVente.setVente(this);
        return this;
    }

    public Vente removeLigneVente(LigneVente ligneVente) {
        this.ligneVentes.remove(ligneVente);
        ligneVente.setVente(null);
        return this;
    }

    public void setLigneVentes(Set<LigneVente> ligneVentes) {
        this.ligneVentes = ligneVentes;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Vente)) {
            return false;
        }
        return id != null && id.equals(((Vente) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Vente{" +
            "id=" + getId() +
            ", dateVente='" + getDateVente() + "'" +
            ", totalHT=" + getTotalHT() +
            ", totalTTC=" + getTotalTTC() +
            "}";
    }

    public void calculTotaux() {
        this.totalHT = (double) 0;
        this.totalTTC = (double) 0;
        for (LigneVente lv : this.ligneVentes) {
            this.totalHT += lv.getSousTotalHT();
            this.totalTTC += lv.getSousTotal();
        }
    }
}
