package com.sophatel.winpharm.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A LigneVente.
 */
@Entity
@Table(name = "ligne_vente")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class LigneVente implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "designation_produit")
    private String designationProduit;

    @Column(name = "prix_produit")
    private Double prixProduit;

    @NotNull
    @Min(value = 0L)
    @Column(name = "quantite", nullable = false)
    private Long quantite;

    @DecimalMin(value = "0")
    @Column(name = "sous_total")
    private Double sousTotal;

    @DecimalMin(value = "0")
    @Column(name = "sous_total_ht")
    private Double sousTotalHT;

    @ManyToOne
    @JsonIgnoreProperties("ligneVentes")
    private Produit produit;

    @ManyToOne
    @JsonIgnoreProperties("ligneVentes")
    private Vente vente;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDesignationProduit() {
        return designationProduit;
    }

    public LigneVente designationProduit(String designationProduit) {
        this.designationProduit = designationProduit;
        return this;
    }

    public void setDesignationProduit(String designationProduit) {
        this.designationProduit = designationProduit;
    }

    public Double getPrixProduit() {
        return prixProduit;
    }

    public LigneVente prixProduit(Double prixProduit) {
        this.prixProduit = prixProduit;
        return this;
    }

    public void setPrixProduit(Double prixProduit) {
        this.prixProduit = prixProduit;
    }

    public Long getQuantite() {
        return quantite;
    }

    public LigneVente quantite(Long quantite) {
        this.quantite = quantite;
        return this;
    }

    public void setQuantite(Long quantite) {
        this.quantite = quantite;
    }

    public Double getSousTotal() {
        return sousTotal;
    }

    public LigneVente sousTotal(Double sousTotal) {
        this.sousTotal = sousTotal;
        return this;
    }

    public void setSousTotal(Double sousTotal) {
        this.sousTotal = sousTotal;
    }

    public Double getSousTotalHT() {
        return sousTotalHT;
    }

    public LigneVente sousTotalHT(Double sousTotalHT) {
        this.sousTotalHT = sousTotalHT;
        return this;
    }

    public void setSousTotalHT(Double sousTotalHT) {
        this.sousTotalHT = sousTotalHT;
    }

    public Produit getProduit() {
        return produit;
    }

    public LigneVente produit(Produit produit) {
        this.produit = produit;
        return this;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }

    public Vente getVente() {
        return vente;
    }

    public LigneVente vente(Vente vente) {
        this.vente = vente;
        return this;
    }

    public void setVente(Vente vente) {
        this.vente = vente;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LigneVente)) {
            return false;
        }
        return id != null && id.equals(((LigneVente) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "LigneVente{" +
            "id=" + getId() +
            ", designationProduit='" + getDesignationProduit() + "'" +
            ", prixProduit=" + getPrixProduit() +
            ", quantite=" + getQuantite() +
            ", sousTotal=" + getSousTotal() +
            ", sousTotalHT=" + getSousTotalHT() +
            "}";
    }
}
