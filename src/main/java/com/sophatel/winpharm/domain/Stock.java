package com.sophatel.winpharm.domain;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A Stock.
 */
@Entity
@Table(name = "stock")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Stock implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Min(value = 0L)
    @Column(name = "couverture_min", nullable = false)
    private Long couvertureMin;

    @NotNull
    @Min(value = 0L)
    @Column(name = "couverture_max", nullable = false)
    private Long couvertureMax;

    @NotNull
    @Min(value = 0L)
    @Column(name = "quantite", nullable = false)
    private Long quantite;

    @NotNull
    @Column(name = "date_peremption", nullable = false)
    private LocalDate datePeremption;

    @NotNull
    @DecimalMin(value = "0")
    @Column(name = "prix_achat", nullable = false)
    private Double prixAchat;

    @NotNull
    @DecimalMin(value = "0")
    @Column(name = "prix_vente", nullable = false)
    private Double prixVente;

    @OneToOne(mappedBy = "stock")
    @JsonIgnore
    private Produit produit;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCouvertureMin() {
        return couvertureMin;
    }

    public Stock couvertureMin(Long couvertureMin) {
        this.couvertureMin = couvertureMin;
        return this;
    }

    public void setCouvertureMin(Long couvertureMin) {
        this.couvertureMin = couvertureMin;
    }

    public Long getCouvertureMax() {
        return couvertureMax;
    }

    public Stock couvertureMax(Long couvertureMax) {
        this.couvertureMax = couvertureMax;
        return this;
    }

    public void setCouvertureMax(Long couvertureMax) {
        this.couvertureMax = couvertureMax;
    }

    public Long getQuantite() {
        return quantite;
    }

    public Stock quantite(Long quantite) {
        this.quantite = quantite;
        return this;
    }

    public void setQuantite(Long quantite) {
        this.quantite = quantite;
    }

    public LocalDate getDatePeremption() {
        return datePeremption;
    }

    public Stock datePeremption(LocalDate datePeremption) {
        this.datePeremption = datePeremption;
        return this;
    }

    public void setDatePeremption(LocalDate datePeremption) {
        this.datePeremption = datePeremption;
    }

    public Double getPrixAchat() {
        return prixAchat;
    }

    public Stock prixAchat(Double prixAchat) {
        this.prixAchat = prixAchat;
        return this;
    }

    public void setPrixAchat(Double prixAchat) {
        this.prixAchat = prixAchat;
    }

    public Double getPrixVente() {
        return prixVente;
    }

    public Stock prixVente(Double prixVente) {
        this.prixVente = prixVente;
        return this;
    }

    public void setPrixVente(Double prixVente) {
        this.prixVente = prixVente;
    }

    public Produit getProduit() {
        return produit;
    }

    public Stock produit(Produit produit) {
        this.produit = produit;
        return this;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Stock)) {
            return false;
        }
        return id != null && id.equals(((Stock) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Stock{" +
            "id=" + getId() +
            ", couvertureMin=" + getCouvertureMin() +
            ", couvertureMax=" + getCouvertureMax() +
            ", quantite=" + getQuantite() +
            ", datePeremption='" + getDatePeremption() + "'" +
            ", prixAchat=" + getPrixAchat() +
            ", prixVente=" + getPrixVente() +
            "}";
    }
}
