package com.sophatel.winpharm.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Produit.
 */
@Entity
@Table(name = "produit")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Produit implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "designation", nullable = false)
    private String designation;

    @Column(name = "actif")
    private Boolean actif;

    @ManyToOne
    @JsonIgnoreProperties("produits")
    private Rayon rayon;

    @ManyToOne
    @JsonIgnoreProperties("produits")
    private Categorie categorie;

    @ManyToOne
    @JsonIgnoreProperties("produits")
    private Laboratoire laboratoire;

    @ManyToOne
    @JsonIgnoreProperties("produits")
    private Grossiste grossiste;

    @OneToOne
    @JoinColumn(unique = true)
    private Stock stock;

    @OneToMany(mappedBy = "produit")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<LigneVente> ligneVentes = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDesignation() {
        return designation;
    }

    public Produit designation(String designation) {
        this.designation = designation;
        return this;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Boolean isActif() {
        return actif;
    }

    public Produit actif(Boolean actif) {
        this.actif = actif;
        return this;
    }

    public void setActif(Boolean actif) {
        this.actif = actif;
    }

    public Rayon getRayon() {
        return rayon;
    }

    public Produit rayon(Rayon rayon) {
        this.rayon = rayon;
        return this;
    }

    public void setRayon(Rayon rayon) {
        this.rayon = rayon;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public Produit categorie(Categorie categorie) {
        this.categorie = categorie;
        return this;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public Laboratoire getLaboratoire() {
        return laboratoire;
    }

    public Produit laboratoire(Laboratoire laboratoire) {
        this.laboratoire = laboratoire;
        return this;
    }

    public void setLaboratoire(Laboratoire laboratoire) {
        this.laboratoire = laboratoire;
    }

    public Grossiste getGrossiste() {
        return grossiste;
    }

    public Produit grossiste(Grossiste grossiste) {
        this.grossiste = grossiste;
        return this;
    }

    public void setGrossiste(Grossiste grossiste) {
        this.grossiste = grossiste;
    }

    public Stock getStock() {
        return stock;
    }

    public Produit stock(Stock stock) {
        this.stock = stock;
        return this;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public Set<LigneVente> getLigneVentes() {
        return ligneVentes;
    }

    public Produit ligneVentes(Set<LigneVente> ligneVentes) {
        this.ligneVentes = ligneVentes;
        return this;
    }

    public Produit addLigneVente(LigneVente ligneVente) {
        this.ligneVentes.add(ligneVente);
        ligneVente.setProduit(this);
        return this;
    }

    public Produit removeLigneVente(LigneVente ligneVente) {
        this.ligneVentes.remove(ligneVente);
        ligneVente.setProduit(null);
        return this;
    }

    public void setLigneVentes(Set<LigneVente> ligneVentes) {
        this.ligneVentes = ligneVentes;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Produit)) {
            return false;
        }
        return id != null && id.equals(((Produit) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Produit{" +
            "id=" + getId() +
            ", designation='" + getDesignation() + "'" +
            ", actif='" + isActif() + "'" +
            "}";
    }
}
