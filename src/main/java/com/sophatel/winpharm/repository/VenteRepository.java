package com.sophatel.winpharm.repository;
import com.sophatel.winpharm.domain.Vente;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Vente entity.
 */
@SuppressWarnings("unused")
@Repository
public interface VenteRepository extends JpaRepository<Vente, Long> {
    @Query("select v from Vente v where to_char(v.dateVente, 'YYYY-MM-DD') like :x")
    public Page<Vente> findAllByDate(@Param("x") String str, Pageable pageable);
}
