package com.sophatel.winpharm.service.impl;

import com.sophatel.winpharm.service.LigneVenteService;
import com.sophatel.winpharm.service.ProduitService;
import com.sophatel.winpharm.service.VenteService;
import com.sophatel.winpharm.domain.LigneVente;
import com.sophatel.winpharm.domain.Produit;
import com.sophatel.winpharm.domain.Stock;
import com.sophatel.winpharm.domain.Vente;
import com.sophatel.winpharm.repository.VenteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.Set;

/**
 * Service Implementation for managing {@link Vente}.
 */
@Service
@Transactional
public class VenteServiceImpl implements VenteService {

    private final Logger log = LoggerFactory.getLogger(VenteServiceImpl.class);

    private final VenteRepository venteRepository;
    private final LigneVenteService ligneVenteService;
    private final ProduitService produitService;

    public VenteServiceImpl(
        VenteRepository venteRepository,
        LigneVenteService ligneVenteService,
        ProduitService produitService
        ) {
        this.venteRepository = venteRepository;
        this.ligneVenteService = ligneVenteService;
        this.produitService = produitService;
    }

    /**
     * Save a vente.
     *
     * @param vente the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Vente save(Vente vente) {
        log.debug("Request to save Vente : {}", vente);
        Set<LigneVente> ligneVentes;
        Produit produit;
        Stock stock;
        double TMP_PRIX_PRODUIT, TMP_TOTAL_HT = 0, TMP_TOTAL_TTC = 0;
        long qte = 0;
        if (vente.getLigneVentes() != null) {
            ligneVentes = vente.getLigneVentes();
            for (LigneVente lv : ligneVentes){
                produit = lv.getProduit();
                stock = produit.getStock();
                qte = lv.getQuantite();
                // tester si la quantité est conforme
                if (produit.getStock().getQuantite() < qte) {
                    qte = produit.getStock().getQuantite();
                }

                lv.setDesignationProduit(produit.getDesignation());
                if (stock != null) {
                    TMP_PRIX_PRODUIT = stock.getPrixVente();
                    TMP_TOTAL_HT = TMP_PRIX_PRODUIT * qte;
                    TMP_TOTAL_TTC = TMP_TOTAL_HT + TMP_TOTAL_HT * 0.07;
                    lv.setPrixProduit(TMP_PRIX_PRODUIT);
                    lv.setSousTotalHT(TMP_TOTAL_HT);
                    lv.setSousTotal(TMP_TOTAL_TTC);
                }

                // enlever la quantite vendue du stock de produit
                produit.getStock().setQuantite(produit.getStock().getQuantite() - qte);
                produitService.save(produit);

                ligneVenteService.save(lv);
                lv.setVente(vente);
            }
        }
        vente.calculTotaux();
        return venteRepository.save(vente);
    }

    /**
     * Get all the ventes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Vente> findAll(Pageable pageable) {
        log.debug("Request to get all Ventes");
        return venteRepository.findAll(pageable);
    }

    /**
     * Get all the ventes by date.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Vente> findAllByDate(String str, Pageable pageable) {
        log.debug("Request to get all Ventes by date");
        return venteRepository.findAllByDate("%"+str+"%", pageable);
    }


    /**
     * Get one vente by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Vente> findOne(Long id) {
        log.debug("Request to get Vente : {}", id);
        Optional<Vente> vente = venteRepository.findById(id);
        Set<LigneVente> ligneVentes = ligneVenteService.findAllByVente(id);
        vente.get().ligneVentes(ligneVentes);
        return vente;
    }

    /**
     * Delete the vente by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Vente : {}", id);
        venteRepository.deleteById(id);
    }
}
