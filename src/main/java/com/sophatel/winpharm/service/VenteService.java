package com.sophatel.winpharm.service;

import com.sophatel.winpharm.domain.Vente;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link Vente}.
 */
public interface VenteService {

    /**
     * Save a vente.
     *
     * @param vente the entity to save.
     * @return the persisted entity.
     */
    Vente save(Vente vente);

    /**
     * Get all the ventes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Vente> findAll(Pageable pageable);

    /**
     * Get all the ventes by date.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Vente> findAllByDate(String str, Pageable pageable);


    /**
     * Get the "id" vente.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Vente> findOne(Long id);

    /**
     * Delete the "id" vente.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
