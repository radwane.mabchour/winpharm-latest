import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'rayon',
        loadChildren: () => import('./rayon/rayon.module').then(m => m.WinpharmRayonModule)
      },
      {
        path: 'categorie',
        loadChildren: () => import('./categorie/categorie.module').then(m => m.WinpharmCategorieModule)
      },
      {
        path: 'laboratoire',
        loadChildren: () => import('./laboratoire/laboratoire.module').then(m => m.WinpharmLaboratoireModule)
      },
      {
        path: 'grossiste',
        loadChildren: () => import('./grossiste/grossiste.module').then(m => m.WinpharmGrossisteModule)
      },
      {
        path: 'stock',
        loadChildren: () => import('./stock/stock.module').then(m => m.WinpharmStockModule)
      },
      {
        path: 'produit',
        loadChildren: () => import('./produit/produit.module').then(m => m.WinpharmProduitModule)
      },
      {
        path: 'vente',
        loadChildren: () => import('./vente/vente.module').then(m => m.WinpharmVenteModule)
      },
      {
        path: 'ligne-vente',
        loadChildren: () => import('./ligne-vente/ligne-vente.module').then(m => m.WinpharmLigneVenteModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ],
  declarations: [],
  entryComponents: [],
  providers: []
})
export class WinpharmEntityModule {}
