import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IGrossiste, Grossiste } from 'app/shared/model/grossiste.model';
import { GrossisteService } from './grossiste.service';

@Component({
  selector: 'jhi-grossiste-update',
  templateUrl: './grossiste-update.component.html'
})
export class GrossisteUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    raisonSociale: [null, [Validators.required]],
    adresse: [null, [Validators.required]],
    tel: [null, [Validators.required]]
  });

  constructor(protected grossisteService: GrossisteService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ grossiste }) => {
      this.updateForm(grossiste);
    });
  }

  updateForm(grossiste: IGrossiste) {
    this.editForm.patchValue({
      id: grossiste.id,
      raisonSociale: grossiste.raisonSociale,
      adresse: grossiste.adresse,
      tel: grossiste.tel
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const grossiste = this.createFromForm();
    if (grossiste.id !== undefined) {
      this.subscribeToSaveResponse(this.grossisteService.update(grossiste));
    } else {
      this.subscribeToSaveResponse(this.grossisteService.create(grossiste));
    }
  }

  private createFromForm(): IGrossiste {
    return {
      ...new Grossiste(),
      id: this.editForm.get(['id']).value,
      raisonSociale: this.editForm.get(['raisonSociale']).value,
      adresse: this.editForm.get(['adresse']).value,
      tel: this.editForm.get(['tel']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IGrossiste>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
