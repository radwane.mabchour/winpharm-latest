import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { WinpharmSharedModule } from 'app/shared/shared.module';
import { GrossisteComponent } from './grossiste.component';
import { GrossisteDetailComponent } from './grossiste-detail.component';
import { GrossisteUpdateComponent } from './grossiste-update.component';
import { GrossisteDeletePopupComponent, GrossisteDeleteDialogComponent } from './grossiste-delete-dialog.component';
import { grossisteRoute, grossistePopupRoute } from './grossiste.route';

const ENTITY_STATES = [...grossisteRoute, ...grossistePopupRoute];

@NgModule({
  imports: [WinpharmSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    GrossisteComponent,
    GrossisteDetailComponent,
    GrossisteUpdateComponent,
    GrossisteDeleteDialogComponent,
    GrossisteDeletePopupComponent
  ],
  entryComponents: [GrossisteComponent, GrossisteUpdateComponent, GrossisteDeleteDialogComponent, GrossisteDeletePopupComponent]
})
export class WinpharmGrossisteModule {}
