import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { WinpharmSharedModule } from 'app/shared/shared.module';
import { ProduitComponent } from './produit.component';
import { ProduitDetailComponent } from './produit-detail.component';
import { ProduitUpdateComponent } from './produit-update.component';
import { ProduitDeletePopupComponent, ProduitDeleteDialogComponent } from './produit-delete-dialog.component';
import { produitRoute, produitPopupRoute } from './produit.route';
import { TabMenuModule } from 'primeng/tabmenu';
import { FieldsetModule } from 'primeng/fieldset';
import { StockComponent } from './stock/stock.component';
import { AutoCompleteModule } from 'primeng/autocomplete';

const ENTITY_STATES = [...produitRoute, ...produitPopupRoute];

@NgModule({
  imports: [WinpharmSharedModule, TabMenuModule, FieldsetModule, AutoCompleteModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    ProduitComponent,
    ProduitDetailComponent,
    ProduitUpdateComponent,
    ProduitDeleteDialogComponent,
    ProduitDeletePopupComponent,
    StockComponent
  ],
  entryComponents: [ProduitComponent, ProduitUpdateComponent, ProduitDeleteDialogComponent, ProduitDeletePopupComponent]
})
export class WinpharmProduitModule {}
