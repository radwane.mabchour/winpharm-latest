import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IProduit, Produit } from 'app/shared/model/produit.model';
import { ProduitService } from './produit.service';
import { IRayon } from 'app/shared/model/rayon.model';
import { RayonService } from 'app/entities/rayon/rayon.service';
import { ICategorie } from 'app/shared/model/categorie.model';
import { CategorieService } from 'app/entities/categorie/categorie.service';
import { ILaboratoire } from 'app/shared/model/laboratoire.model';
import { LaboratoireService } from 'app/entities/laboratoire/laboratoire.service';
import { IGrossiste } from 'app/shared/model/grossiste.model';
import { GrossisteService } from 'app/entities/grossiste/grossiste.service';
import { IStock, Stock } from 'app/shared/model/stock.model';
import { StockService } from 'app/entities/stock/stock.service';

@Component({
  selector: 'jhi-produit-update',
  templateUrl: './produit-update.component.html'
})
export class ProduitUpdateComponent implements OnInit {
  isSaving: boolean;

  rayons: IRayon[];

  categories: ICategorie[];

  laboratoires: ILaboratoire[];

  grossistes: IGrossiste[];

  stocks: IStock[];

  editForm = this.fb.group({
    id: [],
    designation: [null, [Validators.required]],
    actif: [],
    rayon: [],
    categorie: [],
    laboratoire: [],
    grossiste: [],
    stock: []
  });

  public innerStock: IStock = {
    ...new Stock(),
    id: null,
    couvertureMin: 0,
    couvertureMax: 0,
    quantite: 0,
    prixAchat: 0,
    prixVente: 0,
    datePeremption: null,
    produit: null
  };
  public produitStock: IStock;

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected produitService: ProduitService,
    protected rayonService: RayonService,
    protected categorieService: CategorieService,
    protected laboratoireService: LaboratoireService,
    protected grossisteService: GrossisteService,
    protected stockService: StockService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ produit }) => {
      this.updateForm(produit);
    });
    this.rayonService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IRayon[]>) => mayBeOk.ok),
        map((response: HttpResponse<IRayon[]>) => response.body)
      )
      .subscribe((res: IRayon[]) => (this.rayons = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.categorieService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ICategorie[]>) => mayBeOk.ok),
        map((response: HttpResponse<ICategorie[]>) => response.body)
      )
      .subscribe((res: ICategorie[]) => (this.categories = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.laboratoireService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ILaboratoire[]>) => mayBeOk.ok),
        map((response: HttpResponse<ILaboratoire[]>) => response.body)
      )
      .subscribe((res: ILaboratoire[]) => (this.laboratoires = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.grossisteService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IGrossiste[]>) => mayBeOk.ok),
        map((response: HttpResponse<IGrossiste[]>) => response.body)
      )
      .subscribe((res: IGrossiste[]) => (this.grossistes = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.stockService
      .query({ filter: 'produit-is-null' })
      .pipe(
        filter((mayBeOk: HttpResponse<IStock[]>) => mayBeOk.ok),
        map((response: HttpResponse<IStock[]>) => response.body)
      )
      .subscribe(
        (res: IStock[]) => {
          if (!this.editForm.get('stock').value || !this.editForm.get('stock').value.id) {
            this.stocks = res;
          } else {
            this.stockService
              .find(this.editForm.get('stock').value.id)
              .pipe(
                filter((subResMayBeOk: HttpResponse<IStock>) => subResMayBeOk.ok),
                map((subResponse: HttpResponse<IStock>) => subResponse.body)
              )
              .subscribe(
                (subRes: IStock) => (this.stocks = [subRes].concat(res)),
                (subRes: HttpErrorResponse) => this.onError(subRes.message)
              );
          }
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  setInnerStock(data: IStock) {
    this.innerStock.id = data.id;
    this.innerStock.couvertureMin = data.couvertureMin;
    this.innerStock.couvertureMax = data.couvertureMax;
    this.innerStock.quantite = data.quantite;
    this.innerStock.prixAchat = data.prixAchat;
    this.innerStock.prixVente = data.prixVente;
    this.innerStock.datePeremption = data.datePeremption;
    // save produit with his stock
    this.save();
  }

  updateForm(produit: IProduit) {
    this.editForm.patchValue({
      id: produit.id,
      designation: produit.designation,
      actif: produit.actif,
      rayon: produit.rayon,
      categorie: produit.categorie,
      laboratoire: produit.laboratoire,
      grossiste: produit.grossiste,
      stock: produit.stock
    });
    if (produit.stock) {
      // const fromProdToStock: IStock = {
      //   ...new Stock(),
      //   id: produit.stock.id,
      //   couvertureMin: produit.stock.couvertureMin,
      //   couvertureMax: produit.stock.couvertureMax,
      //   quantite: produit.stock.quantite,
      //   prixAchat: produit.stock.prixAchat,
      //   prixVente: produit.stock.prixVente,
      //   datePeremption: produit.stock.datePeremption
      // };
      this.produitStock = produit.stock;
    }
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const produit = this.createFromForm();
    if (produit.id !== undefined) {
      this.subscribeToSaveResponse(this.produitService.update(produit));
    } else {
      if (produit.stock.id !== undefined) {
        this.subscribeToSaveResponse(this.produitService.update(produit));
      }
      this.subscribeToSaveResponse(this.produitService.create(produit));
    }
  }

  private createFromForm(): IProduit {
    const produit: IProduit = {
      ...new Produit(),
      id: this.editForm.get(['id']).value,
      designation: this.editForm.get(['designation']).value,
      actif: this.editForm.get(['actif']).value,
      rayon: this.editForm.get(['rayon']).value,
      categorie: this.editForm.get(['categorie']).value,
      laboratoire: this.editForm.get(['laboratoire']).value,
      grossiste: this.editForm.get(['grossiste']).value,
      stock: this.innerStock
    };
    if (this.innerStock) {
      if (this.innerStock.quantite > 0) {
        produit.actif = true;
      } else {
        produit.actif = false;
      }
    }
    return produit;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IProduit>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackRayonById(index: number, item: IRayon) {
    return item.id;
  }

  trackCategorieById(index: number, item: ICategorie) {
    return item.id;
  }

  trackLaboratoireById(index: number, item: ILaboratoire) {
    return item.id;
  }

  trackGrossisteById(index: number, item: IGrossiste) {
    return item.id;
  }

  trackStockById(index: number, item: IStock) {
    return item.id;
  }
}
