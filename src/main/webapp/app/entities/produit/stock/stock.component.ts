import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';
import { IStock, Stock } from 'app/shared/model/stock.model';
import { StockService } from 'app/entities/stock/stock.service';
import { IProduit } from 'app/shared/model/produit.model';
import * as moment from 'moment';

@Component({
  selector: 'jhi-stock',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.scss']
})
export class StockComponent implements OnInit {
  isSaving: boolean;

  produits: IProduit[];
  datePeremptionDp: any;

  editForm = this.fb.group({
    id: [],
    couvertureMin: [null, [Validators.required, Validators.min(0)]],
    couvertureMax: [null, [Validators.required, Validators.min(0)]],
    quantite: [null, [Validators.required, Validators.min(0)]],
    datePeremption: [null, [Validators.required]],
    prixAchat: [null, [Validators.required, Validators.min(0)]],
    prixVente: [null, [Validators.required, Validators.min(0)]]
  });

  @Input() stockFromProduit: IStock;
  @Output() stockEvent = new EventEmitter<IStock>();

  public couvertureMin: number;
  public couvertureMax: number;

  public emptyStock: IStock = {
    ...new Stock(),
    id: null,
    couvertureMin: 0,
    couvertureMax: 0,
    quantite: 0,
    prixAchat: 0,
    prixVente: 0,
    datePeremption: null,
    produit: null
  };

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected stockService: StockService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    if (this.stockFromProduit) {
      this.updateForm(this.stockFromProduit);
    } else {
      this.updateForm(this.emptyStock);
    }
  }

  updateForm(stock: IStock) {
    this.editForm.patchValue({
      id: stock.id,
      couvertureMin: stock.couvertureMin,
      couvertureMax: stock.couvertureMax,
      quantite: stock.quantite,
      datePeremption: moment.utc(stock.datePeremption),
      prixAchat: stock.prixAchat,
      prixVente: stock.prixVente
    });
    this.couvertureMin = stock.couvertureMin;
    this.couvertureMax = stock.couvertureMax;
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const stock = this.createFromForm();
    if (stock != null) {
      console.log('stock : ');
      console.log(stock);
      if (stock.id !== undefined) {
        this.subscribeToSaveResponse(this.stockService.update(stock));
      } else {
        this.subscribeToSaveResponse(this.stockService.create(stock));
      }
      this.stockEvent.emit(stock);
    }
  }

  private createFromForm(): IStock {
    if (this.editForm.get(['couvertureMin']).value < this.editForm.get(['couvertureMax']).value) {
      return {
        ...new Stock(),
        id: this.editForm.get(['id']).value,
        couvertureMin: this.editForm.get(['couvertureMin']).value,
        couvertureMax: this.editForm.get(['couvertureMax']).value,
        quantite: this.editForm.get(['quantite']).value,
        datePeremption: this.editForm.get(['datePeremption']).value,
        prixAchat: this.editForm.get(['prixAchat']).value,
        prixVente: this.editForm.get(['prixVente']).value
      };
    }
    return null;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IStock>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    // this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
