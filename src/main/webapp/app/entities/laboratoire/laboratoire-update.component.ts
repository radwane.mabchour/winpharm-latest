import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ILaboratoire, Laboratoire } from 'app/shared/model/laboratoire.model';
import { LaboratoireService } from './laboratoire.service';

@Component({
  selector: 'jhi-laboratoire-update',
  templateUrl: './laboratoire-update.component.html'
})
export class LaboratoireUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    raisonSociale: [null, [Validators.required]],
    adresse: [null, [Validators.required]],
    tel: [null, [Validators.required]]
  });

  constructor(protected laboratoireService: LaboratoireService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ laboratoire }) => {
      this.updateForm(laboratoire);
    });
  }

  updateForm(laboratoire: ILaboratoire) {
    this.editForm.patchValue({
      id: laboratoire.id,
      raisonSociale: laboratoire.raisonSociale,
      adresse: laboratoire.adresse,
      tel: laboratoire.tel
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const laboratoire = this.createFromForm();
    if (laboratoire.id !== undefined) {
      this.subscribeToSaveResponse(this.laboratoireService.update(laboratoire));
    } else {
      this.subscribeToSaveResponse(this.laboratoireService.create(laboratoire));
    }
  }

  private createFromForm(): ILaboratoire {
    return {
      ...new Laboratoire(),
      id: this.editForm.get(['id']).value,
      raisonSociale: this.editForm.get(['raisonSociale']).value,
      adresse: this.editForm.get(['adresse']).value,
      tel: this.editForm.get(['tel']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ILaboratoire>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
