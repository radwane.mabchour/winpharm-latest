import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { WinpharmSharedModule } from 'app/shared/shared.module';
import { LigneVenteComponent } from './ligne-vente.component';
import { LigneVenteDetailComponent } from './ligne-vente-detail.component';
import { LigneVenteUpdateComponent } from './ligne-vente-update.component';
import { LigneVenteDeletePopupComponent, LigneVenteDeleteDialogComponent } from './ligne-vente-delete-dialog.component';
import { ligneVenteRoute, ligneVentePopupRoute } from './ligne-vente.route';

const ENTITY_STATES = [...ligneVenteRoute, ...ligneVentePopupRoute];

@NgModule({
  imports: [WinpharmSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    LigneVenteComponent,
    LigneVenteDetailComponent,
    LigneVenteUpdateComponent,
    LigneVenteDeleteDialogComponent,
    LigneVenteDeletePopupComponent
  ],
  entryComponents: [LigneVenteComponent, LigneVenteUpdateComponent, LigneVenteDeleteDialogComponent, LigneVenteDeletePopupComponent]
})
export class WinpharmLigneVenteModule {}
