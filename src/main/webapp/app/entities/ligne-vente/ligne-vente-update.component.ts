import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ILigneVente, LigneVente } from 'app/shared/model/ligne-vente.model';
import { LigneVenteService } from './ligne-vente.service';
import { IProduit } from 'app/shared/model/produit.model';
import { ProduitService } from 'app/entities/produit/produit.service';
import { IVente } from 'app/shared/model/vente.model';
import { VenteService } from 'app/entities/vente/vente.service';

@Component({
  selector: 'jhi-ligne-vente-update',
  templateUrl: './ligne-vente-update.component.html'
})
export class LigneVenteUpdateComponent implements OnInit {
  isSaving: boolean;

  produits: IProduit[];

  ventes: IVente[];

  editForm = this.fb.group({
    id: [],
    designationProduit: [],
    prixProduit: [],
    quantite: [null, [Validators.required, Validators.min(0)]],
    sousTotal: [null, [Validators.min(0)]],
    sousTotalHT: [null, [Validators.min(0)]],
    produit: [],
    vente: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected ligneVenteService: LigneVenteService,
    protected produitService: ProduitService,
    protected venteService: VenteService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ ligneVente }) => {
      this.updateForm(ligneVente);
    });
    this.produitService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IProduit[]>) => mayBeOk.ok),
        map((response: HttpResponse<IProduit[]>) => response.body)
      )
      .subscribe((res: IProduit[]) => (this.produits = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.venteService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IVente[]>) => mayBeOk.ok),
        map((response: HttpResponse<IVente[]>) => response.body)
      )
      .subscribe((res: IVente[]) => (this.ventes = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(ligneVente: ILigneVente) {
    this.editForm.patchValue({
      id: ligneVente.id,
      designationProduit: ligneVente.designationProduit,
      prixProduit: ligneVente.prixProduit,
      quantite: ligneVente.quantite,
      sousTotal: ligneVente.sousTotal,
      sousTotalHT: ligneVente.sousTotalHT,
      produit: ligneVente.produit,
      vente: ligneVente.vente
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const ligneVente = this.createFromForm();
    if (ligneVente.id !== undefined) {
      this.subscribeToSaveResponse(this.ligneVenteService.update(ligneVente));
    } else {
      this.subscribeToSaveResponse(this.ligneVenteService.create(ligneVente));
    }
  }

  private createFromForm(): ILigneVente {
    return {
      ...new LigneVente(),
      id: this.editForm.get(['id']).value,
      designationProduit: this.editForm.get(['designationProduit']).value,
      prixProduit: this.editForm.get(['prixProduit']).value,
      quantite: this.editForm.get(['quantite']).value,
      sousTotal: this.editForm.get(['sousTotal']).value,
      sousTotalHT: this.editForm.get(['sousTotalHT']).value,
      produit: this.editForm.get(['produit']).value,
      vente: this.editForm.get(['vente']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ILigneVente>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackProduitById(index: number, item: IProduit) {
    return item.id;
  }

  trackVenteById(index: number, item: IVente) {
    return item.id;
  }
}
