import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IStock, Stock } from 'app/shared/model/stock.model';
import { StockService } from './stock.service';
import { IProduit } from 'app/shared/model/produit.model';
import { ProduitService } from 'app/entities/produit/produit.service';

@Component({
  selector: 'jhi-stock-update',
  templateUrl: './stock-update.component.html'
})
export class StockUpdateComponent implements OnInit {
  isSaving: boolean;

  produits: IProduit[];
  datePeremptionDp: any;

  editForm = this.fb.group({
    id: [],
    couvertureMin: [null, [Validators.required, Validators.min(0)]],
    couvertureMax: [null, [Validators.required, Validators.min(0)]],
    quantite: [null, [Validators.required, Validators.min(0)]],
    datePeremption: [null, [Validators.required]],
    prixAchat: [null, [Validators.required, Validators.min(0)]],
    prixVente: [null, [Validators.required, Validators.min(0)]]
  });

  @Input() stockFromProduit: IStock;
  @Output() stockEvent = new EventEmitter<IStock>();
  public stockObj: IStock;

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected stockService: StockService,
    protected produitService: ProduitService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ stock }) => {
      if (this.stockFromProduit) {
        // eslint-disable-next-line no-console
        console.log('stock component : from Produit -> Stock : ');
        // eslint-disable-next-line no-console
        console.log(this.stockFromProduit);
        this.updateForm(this.stockFromProduit);
      } else {
        // eslint-disable-next-line no-console
        console.log('edit stock : ');
        // eslint-disable-next-line no-console
        console.log('stock-log : ' + stock);
        this.updateForm(stock);
      }
    });
    this.produitService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IProduit[]>) => mayBeOk.ok),
        map((response: HttpResponse<IProduit[]>) => response.body)
      )
      .subscribe((res: IProduit[]) => (this.produits = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  sendStockToProduit() {
    this.stockObj = this.createFromForm();
    this.stockEvent.emit(this.stockObj);
  }

  updateForm(stock: IStock) {
    if (stock) {
      this.editForm.patchValue({
        id: stock.id,
        couvertureMin: stock.couvertureMin,
        couvertureMax: stock.couvertureMax,
        quantite: stock.quantite,
        datePeremption: stock.datePeremption,
        prixAchat: stock.prixAchat,
        prixVente: stock.prixVente
      });
      console.log('stock-log-2 : ');
      console.log(stock);
      console.log('date-perem : ');
      console.log(stock.datePeremption);
    } else {
      this.editForm.patchValue({
        id: null,
        couvertureMin: 0,
        couvertureMax: 0,
        quantite: 0,
        datePeremption: null,
        prixAchat: 0,
        prixVente: 0
      });
    }
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const stock = this.createFromForm();
    if (stock.id !== undefined) {
      this.subscribeToSaveResponse(this.stockService.update(stock));
    } else {
      this.subscribeToSaveResponse(this.stockService.create(stock));
    }
  }

  private createFromForm(): IStock {
    return {
      ...new Stock(),
      id: this.editForm.get(['id']).value,
      couvertureMin: this.editForm.get(['couvertureMin']).value,
      couvertureMax: this.editForm.get(['couvertureMax']).value,
      quantite: this.editForm.get(['quantite']).value,
      datePeremption: this.editForm.get(['datePeremption']).value,
      prixAchat: this.editForm.get(['prixAchat']).value,
      prixVente: this.editForm.get(['prixVente']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IStock>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackProduitById(index: number, item: IProduit) {
    return item.id;
  }
}
