import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { WinpharmSharedModule } from 'app/shared/shared.module';
import { CategorieComponent } from './categorie.component';
import { CategorieDetailComponent } from './categorie-detail.component';
import { CategorieUpdateComponent } from './categorie-update.component';
import { CategorieDeletePopupComponent, CategorieDeleteDialogComponent } from './categorie-delete-dialog.component';
import { categorieRoute, categoriePopupRoute } from './categorie.route';

const ENTITY_STATES = [...categorieRoute, ...categoriePopupRoute];

@NgModule({
  imports: [WinpharmSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    CategorieComponent,
    CategorieDetailComponent,
    CategorieUpdateComponent,
    CategorieDeleteDialogComponent,
    CategorieDeletePopupComponent
  ],
  entryComponents: [CategorieComponent, CategorieUpdateComponent, CategorieDeleteDialogComponent, CategorieDeletePopupComponent]
})
export class WinpharmCategorieModule {}
