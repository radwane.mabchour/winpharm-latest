import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { WinpharmSharedModule } from 'app/shared/shared.module';
import { RayonComponent } from './rayon.component';
import { RayonDetailComponent } from './rayon-detail.component';
import { RayonUpdateComponent } from './rayon-update.component';
import { RayonDeletePopupComponent, RayonDeleteDialogComponent } from './rayon-delete-dialog.component';
import { rayonRoute, rayonPopupRoute } from './rayon.route';

const ENTITY_STATES = [...rayonRoute, ...rayonPopupRoute];

@NgModule({
  imports: [WinpharmSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [RayonComponent, RayonDetailComponent, RayonUpdateComponent, RayonDeleteDialogComponent, RayonDeletePopupComponent],
  entryComponents: [RayonComponent, RayonUpdateComponent, RayonDeleteDialogComponent, RayonDeletePopupComponent]
})
export class WinpharmRayonModule {}
