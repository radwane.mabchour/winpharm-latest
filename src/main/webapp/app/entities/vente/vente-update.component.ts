import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IVente, Vente } from 'app/shared/model/vente.model';
import { VenteService } from './vente.service';
import { ILigneVente, LigneVente } from 'app/shared/model/ligne-vente.model';
import { IProduit } from 'app/shared/model/produit.model';
import { LigneVenteService } from '../ligne-vente/ligne-vente.service';
import { ProduitService } from '../produit/produit.service';

@Component({
  selector: 'jhi-vente-update',
  templateUrl: './vente-update.component.html',
  styleUrls: ['./vente-update.component.scss']
})
export class VenteUpdateComponent implements OnInit {
  isSaving: boolean;
  dateVenteDp: any;

  ligneVentes: ILigneVente[];
  produits: IProduit[];

  totalHT = 0;
  totalTTC = 0;

  editForm = this.fb.group({
    id: [],
    dateVente: [null, [Validators.required]],
    totalHT: [null, [Validators.required, Validators.min(0)]],
    totalTTC: [null, [Validators.required, Validators.min(0)]]
  });

  public listLV: ILigneVente[] = new Array<ILigneVente>();
  public lvsFromVente: ILigneVente[];
  // les IDs des lignes de ventes supprimées !
  public deletedLVs: number[] = [];

  constructor(
    protected venteService: VenteService,
    protected activatedRoute: ActivatedRoute,
    protected ligneVenteService: LigneVenteService,
    protected produitService: ProduitService,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ vente }) => {
      this.updateForm(vente);
    });
  }

  updateForm(vente: IVente) {
    this.editForm.patchValue({
      id: vente.id,
      dateVente: vente.dateVente,
      totalHT: vente.totalHT ? vente.totalHT : this.totalHT,
      totalTTC: vente.totalTTC ? vente.totalTTC : this.totalTTC
    });
    this.totalHT = vente.totalHT ? vente.totalHT : 0;
    this.totalTTC = vente.totalTTC ? vente.totalTTC : 0;
    this.lvsFromVente = vente.ligneVentes;
    this.listLV = vente.ligneVentes;
    console.log('lvs-from-vente : ' + JSON.stringify(this.lvsFromVente));
  }

  calculTotaux(data: ILigneVente[]) {
    this.totalHT = 0;
    this.totalTTC = 0;
    for (let i = 0; i < data.length; i++) {
      this.totalHT += data[i].sousTotalHT;
      this.totalTTC += data[i].sousTotal;
    }
    this.listLV = data;
  }

  sendLV(data: ILigneVente) {
    console.log('Vente : sendLV() : ' + JSON.stringify(data));
    if (this.listLV == null) {
      this.listLV = new Array<ILigneVente>();
    }
    // test si le produit exist deja
    for (let i = 0; i < this.listLV.length; i++) {
      if (this.listLV[i].produit.designation === data.produit.designation) {
        if (this.listLV[i].quantite + data.quantite > data.produit.stock.quantite) {
          this.listLV[i].quantite = data.produit.stock.quantite;
          if (data.produit.stock.quantite - this.listLV[i].quantite > 0) {
            data.quantite = data.produit.stock.quantite - this.listLV[i].quantite;
          }
        } else {
          this.listLV[i].quantite += data.quantite;
        }
        return;
      }
    }
    // add ligne-vente to the list
    const TMP_TOTAL_HT = data.quantite * data.produit.stock.prixVente;
    const tmpLV = {
      ...new LigneVente(),
      id: null,
      designationProduit: data.produit.designation,
      prixProduit: data.produit.stock.prixVente,
      sousTotal: TMP_TOTAL_HT + TMP_TOTAL_HT * 0.07,
      sousTotalHT: TMP_TOTAL_HT,
      quantite: data.quantite,
      produit: data.produit,
      vente: null
    };
    this.listLV.push(tmpLV);
    this.totalHT += tmpLV.sousTotalHT;
    this.totalTTC += tmpLV.sousTotal;
  }

  deleteLV(ids: number[]) {
    console.log('produit id received to be deleted - id : ' + ids[0]);
    if (ids.length > 1) {
      console.log('ligne-vente id received to be deleted - id : ' + ids[0]);
      this.deletedLVs.push(ids[1]);
      console.log('deletedLVs : ' + this.deletedLVs);
    }
    if (this.listLV != null) {
      for (let i = 0; i < this.listLV.length; i++) {
        if (this.listLV[i].produit.id === ids[0]) {
          const tmpLV = this.listLV[i];
          this.totalHT -= tmpLV.sousTotalHT;
          this.totalTTC -= tmpLV.sousTotal;
          this.listLV.splice(i, 1);
          return;
        }
      }
    }
  }

  previousState() {
    window.history.back();
  }

  save() {
    console.log('**************** saving vente ****************');
    this.isSaving = true;
    const vente = this.createFromForm();

    // delete dump LVs
    for (let i = 0; i < this.deletedLVs.length; i++) {
      console.log('delete LV - id : ' + this.deletedLVs[i]);
      this.ligneVenteService.delete(this.deletedLVs[i]).subscribe(response => {
        console.log('deleting ligne-vente ... ');
      });
      console.log('deleted LV - id : ' + this.deletedLVs[i]);
    }

    if (vente.id !== undefined) {
      this.subscribeToSaveResponse(this.venteService.update(vente));
    } else {
      this.subscribeToSaveResponse(this.venteService.create(vente));
    }
  }

  private createFromForm(): IVente {
    return {
      ...new Vente(),
      id: this.editForm.get(['id']).value,
      dateVente: this.editForm.get(['dateVente']).value,
      totalHT: this.editForm.get(['totalHT']).value,
      totalTTC: this.editForm.get(['totalTTC']).value,
      ligneVentes: this.listLV
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IVente>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
