import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { WinpharmSharedModule } from 'app/shared/shared.module';
import { VenteComponent } from './vente.component';
import { VenteDetailComponent } from './vente-detail.component';
import { VenteUpdateComponent } from './vente-update.component';
import { VenteDeletePopupComponent, VenteDeleteDialogComponent } from './vente-delete-dialog.component';
import { venteRoute, ventePopupRoute } from './vente.route';
import { LigneVenteComponent } from './ligne-vente/ligne-vente.component';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { CalendarModule } from 'primeng/calendar';

const ENTITY_STATES = [...venteRoute, ...ventePopupRoute];

@NgModule({
  imports: [WinpharmSharedModule, AutoCompleteModule, RouterModule.forChild(ENTITY_STATES), CalendarModule],
  declarations: [
    VenteComponent,
    VenteDetailComponent,
    VenteUpdateComponent,
    VenteDeleteDialogComponent,
    VenteDeletePopupComponent,
    LigneVenteComponent
  ],
  entryComponents: [VenteComponent, VenteUpdateComponent, VenteDeleteDialogComponent, VenteDeletePopupComponent]
})
export class WinpharmVenteModule {}
