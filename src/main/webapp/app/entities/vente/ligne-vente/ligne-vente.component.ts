import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService, JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { ILigneVente, LigneVente } from 'app/shared/model/ligne-vente.model';
import { LigneVenteService } from 'app/entities/ligne-vente/ligne-vente.service';
import { IProduit } from 'app/shared/model/produit.model';
import { ProduitService } from 'app/entities/produit/produit.service';
import { IVente } from 'app/shared/model/vente.model';
import { IStock, Stock } from 'app/shared/model/stock.model';
import * as moment from 'moment';

@Component({
  selector: 'jhi-ligne-vente',
  templateUrl: './ligne-vente.component.html',
  styleUrls: ['./ligne-vente.component.scss']
})
export class LigneVenteComponent implements OnInit {
  currentAccount: any;
  error: any;
  success: any;
  eventSubscriber: Subscription;
  routeData: any;
  links: any;
  totalItems: any;
  itemsPerPage: any;
  page: any;
  predicate: any;
  previousPage: any;
  reverse: any;

  isSaving: boolean;

  produits: IProduit[];

  ligneVentes: ILigneVente[];

  editForm = this.fb.group({
    id: [],
    designationProduit: [],
    prixProduit: [],
    quantite: [null, [Validators.required, Validators.min(1)]],
    sousTotal: [null, [Validators.min(0)]],
    sousTotalHT: [null, [Validators.min(0)]],
    produit: [],
    vente: []
  });

  @Input() lvFromVente: ILigneVente[];
  @Input() ligneVente: ILigneVente;
  @Output() sendLVs = new EventEmitter<ILigneVente>();
  @Output() produitID = new EventEmitter<number[]>();
  @Output() notify = new EventEmitter<ILigneVente[]>();

  public lvSample: ILigneVente;
  public myStock: IStock = new Stock();
  private qteOldValue: number;

  public emptyLV: ILigneVente = {
    ...new LigneVente(),
    id: null,
    designationProduit: null,
    prixProduit: 0,
    sousTotal: 0,
    sousTotalHT: 0,
    quantite: 0,
    produit: null,
    vente: null
  };

  autoProduit: IProduit;
  results: IProduit[] = [];

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected parseLinks: JhiParseLinks,
    protected ligneVenteService: LigneVenteService,
    protected produitService: ProduitService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    if (this.lvFromVente) {
      console.log('lv-from-vente : ' + JSON.stringify(this.lvFromVente));
      this.ligneVentes = this.lvFromVente;
    }
    if (this.ligneVente) {
      this.updateForm(this.ligneVente);
    } else {
      this.updateForm(this.emptyLV);
    }
    this.produitService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IProduit[]>) => mayBeOk.ok),
        map((response: HttpResponse<IProduit[]>) => response.body)
      )
      .subscribe((res: IProduit[]) => (this.produits = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.registerChangeInLigneVentes();
  }

  search(event) {
    this.results = [];
    this.produitService
      .query({
        q: event.query
      })
      .subscribe(
        (res: HttpResponse<IProduit[]>) => {
          const tmpResult = res.body;
          const today = moment();
          tmpResult.forEach((p, index, tmpArray) => {
            if (!p.actif || p.stock.datePeremption < today) {
              tmpResult.splice(index, 1);
            }
          });
          this.results = tmpResult;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
    console.log('result : ');
    console.log(this.results);
  }

  updateForm(ligneVente: ILigneVente) {
    this.editForm.patchValue({
      id: ligneVente.id,
      designationProduit: ligneVente.designationProduit,
      prixProduit: ligneVente.prixProduit,
      quantite: ligneVente.quantite,
      sousTotal: ligneVente.sousTotal,
      sousTotalHT: ligneVente.sousTotalHT,
      produit: ligneVente.produit,
      vente: ligneVente.vente
    });
  }

  addLigneVente() {
    this.lvSample = this.createFromForm();
    this.lvSample.designationProduit = this.lvSample.produit.designation;

    // tester si la quantite demandee est convenable
    const tmpProduit = this.lvSample.produit;
    console.log(tmpProduit);
    if (tmpProduit.stock.quantite < this.lvSample.quantite) {
      this.lvSample.quantite = tmpProduit.stock.quantite;
    }
    console.log(this.lvSample.quantite);
    this.qteOldValue = this.lvSample.quantite;
    this.sendLVs.emit(this.lvSample);
  }

  sendProduitDeleteRequest(id: number, idLV?: number) {
    console.log('produit delete btn clicked - id : ' + id);
    if (idLV) {
      console.log('ligne-vente delete btn clicked - id : ' + idLV);
    }
    this.produitID.emit([id, idLV]);
    for (let i = 0; this.ligneVentes != null && i < this.ligneVentes.length; i++) {
      if (this.ligneVentes[i].produit.id === id) {
        this.ligneVentes.splice(i, 1);
        console.log('ligne-vente deleted !!');
        return;
      }
    }
  }

  previousState() {
    window.history.back();
  }

  save() {
    console.log('***************** starting save ligne-vente *****************');
    this.isSaving = true;
    const ligneVente = this.createFromForm();
    console.log('ligne-vente : ' + JSON.stringify(ligneVente));
    if (ligneVente.produit.stock.quantite < ligneVente.quantite) {
      ligneVente.quantite = ligneVente.produit.stock.quantite;
      ligneVente.sousTotalHT = ligneVente.quantite * ligneVente.produit.stock.prixVente;
      ligneVente.sousTotal = ligneVente.sousTotalHT + ligneVente.sousTotalHT * 0.07;
    }
    if (this.ligneVentes == null) {
      this.ligneVentes = [ligneVente];
    } else {
      for (let i = 0; i < this.ligneVentes.length; i++) {
        const tmpProduit = this.ligneVentes[i].produit;
        if (this.ligneVentes[i].produit.designation === ligneVente.produit.designation) {
          if (this.ligneVentes[i].quantite === this.qteOldValue) {
            // tester si la quantite demandée est convenable
            if (tmpProduit.stock.quantite < this.ligneVentes[i].quantite + ligneVente.quantite) {
              this.ligneVentes[i].quantite = tmpProduit.stock.quantite;
              this.onWarning("La quantité demandée n'est pas disponible ! On a fixé la quantité à " + tmpProduit.stock.quantite);
            } else {
              this.ligneVentes[i].quantite += ligneVente.quantite;
            }
          }
          this.ligneVentes[i].sousTotalHT = this.ligneVentes[i].quantite * this.ligneVentes[i].produit.stock.prixVente;
          this.ligneVentes[i].sousTotal = this.ligneVentes[i].sousTotalHT + this.ligneVentes[i].sousTotalHT * 0.07;
          this.notify.emit(this.ligneVentes);
          return;
        }
      }
      this.ligneVentes.push(ligneVente);
    }
    console.log('after-save !!');
    console.log('this.ligneVentes : ' + JSON.stringify(this.ligneVentes));
    // if (ligneVente.id !== undefined) {
    //   this.subscribeToSaveResponse(this.ligneVenteService.update(ligneVente));
    // } else {
    //   this.subscribeToSaveResponse(this.ligneVenteService.create(ligneVente));
    // }
  }

  private createFromForm(): ILigneVente {
    const myLV: ILigneVente = {
      ...new LigneVente(),
      id: this.editForm.get(['id']).value,
      designationProduit: null,
      prixProduit: 0,
      quantite: this.editForm.get(['quantite']).value,
      sousTotal: 0,
      sousTotalHT: 0,
      produit: this.editForm.get(['produit']).value,
      vente: this.editForm.get(['vente']).value
    };
    myLV.designationProduit = myLV.produit.designation;
    myLV.prixProduit = myLV.produit.stock.prixVente;

    if (myLV.produit.stock.quantite < myLV.quantite) {
      myLV.quantite = myLV.produit.stock.quantite;
      this.onWarning("La quantité demandée n'est pas disponible ! On a fixé la quantité à " + myLV.quantite);
    }

    myLV.sousTotalHT = myLV.quantite * myLV.produit.stock.prixVente;
    myLV.sousTotal = myLV.sousTotalHT + myLV.sousTotalHT * 0.07;

    return myLV;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ILigneVente>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    // this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
  protected onWarning(errorMessage: string) {
    this.jhiAlertService.warning(errorMessage, null, null);
  }

  trackProduitById(index: number, item: IProduit) {
    return item.id;
  }

  trackVenteById(index: number, item: IVente) {
    return item.id;
  }

  // tslint:disable-next-line: use-lifecycle-interface
  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: ILigneVente) {
    return item.id;
  }

  registerChangeInLigneVentes() {
    this.eventSubscriber = this.eventManager.subscribe('ligneVenteListModification', response => this.loadAll());
  }

  protected paginateLigneVentes(data: ILigneVente[], headers: HttpHeaders) {
    this.links = this.parseLinks.parse(headers.get('link'));
    this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
    this.ligneVentes = data;
  }

  loadAll() {
    this.ligneVenteService
      .query({
        page: this.page - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<ILigneVente[]>) => this.paginateLigneVentes(res.body, res.headers),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.transition();
    }
  }

  transition() {
    this.router.navigate(['/ligne-vente'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    });
    this.loadAll();
  }

  clear() {
    this.page = 0;
    this.router.navigate([
      '/ligne-vente',
      {
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    ]);
    this.loadAll();
  }

  sort() {
    const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }
}
