import { IProduit } from 'app/shared/model/produit.model';
import { IVente } from 'app/shared/model/vente.model';

export interface ILigneVente {
  id?: number;
  designationProduit?: string;
  prixProduit?: number;
  quantite?: number;
  sousTotal?: number;
  sousTotalHT?: number;
  produit?: IProduit;
  vente?: IVente;
}

export class LigneVente implements ILigneVente {
  constructor(
    public id?: number,
    public designationProduit?: string,
    public prixProduit?: number,
    public quantite?: number,
    public sousTotal?: number,
    public sousTotalHT?: number,
    public produit?: IProduit,
    public vente?: IVente
  ) {}
}
