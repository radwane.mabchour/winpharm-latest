import { Moment } from 'moment';
import { ILigneVente } from 'app/shared/model/ligne-vente.model';

export interface IVente {
  id?: number;
  dateVente?: Moment;
  totalHT?: number;
  totalTTC?: number;
  ligneVentes?: ILigneVente[];
}

export class Vente implements IVente {
  constructor(
    public id?: number,
    public dateVente?: Moment,
    public totalHT?: number,
    public totalTTC?: number,
    public ligneVentes?: ILigneVente[]
  ) {}
}
