import { IRayon } from 'app/shared/model/rayon.model';
import { ICategorie } from 'app/shared/model/categorie.model';
import { ILaboratoire } from 'app/shared/model/laboratoire.model';
import { IGrossiste } from 'app/shared/model/grossiste.model';
import { IStock } from 'app/shared/model/stock.model';
import { ILigneVente } from 'app/shared/model/ligne-vente.model';

export interface IProduit {
  id?: number;
  designation?: string;
  actif?: boolean;
  rayon?: IRayon;
  categorie?: ICategorie;
  laboratoire?: ILaboratoire;
  grossiste?: IGrossiste;
  stock?: IStock;
  ligneVentes?: ILigneVente[];
}

export class Produit implements IProduit {
  constructor(
    public id?: number,
    public designation?: string,
    public actif?: boolean,
    public rayon?: IRayon,
    public categorie?: ICategorie,
    public laboratoire?: ILaboratoire,
    public grossiste?: IGrossiste,
    public stock?: IStock,
    public ligneVentes?: ILigneVente[]
  ) {
    this.actif = this.actif || false;
  }
}
