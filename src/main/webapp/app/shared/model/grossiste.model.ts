import { IProduit } from 'app/shared/model/produit.model';

export interface IGrossiste {
  id?: number;
  raisonSociale?: string;
  adresse?: string;
  tel?: number;
  produits?: IProduit[];
}

export class Grossiste implements IGrossiste {
  constructor(
    public id?: number,
    public raisonSociale?: string,
    public adresse?: string,
    public tel?: number,
    public produits?: IProduit[]
  ) {}
}
