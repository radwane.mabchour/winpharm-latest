import { IProduit } from 'app/shared/model/produit.model';

export interface IRayon {
  id?: number;
  libelle?: string;
  produits?: IProduit[];
}

export class Rayon implements IRayon {
  constructor(public id?: number, public libelle?: string, public produits?: IProduit[]) {}
}
