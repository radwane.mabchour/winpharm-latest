import { Moment } from 'moment';
import { IProduit } from 'app/shared/model/produit.model';

export interface IStock {
  id?: number;
  couvertureMin?: number;
  couvertureMax?: number;
  quantite?: number;
  datePeremption?: Moment;
  prixAchat?: number;
  prixVente?: number;
  produit?: IProduit;
}

export class Stock implements IStock {
  constructor(
    public id?: number,
    public couvertureMin?: number,
    public couvertureMax?: number,
    public quantite?: number,
    public datePeremption?: Moment,
    public prixAchat?: number,
    public prixVente?: number,
    public produit?: IProduit
  ) {}
}
